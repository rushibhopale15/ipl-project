const path = require('path');
const csvtojson = require('csvtojson');

const matches = path.join(__dirname, '../data/matches.csv');
const deliveries = path.join(__dirname, '../data/deliveries.csv');

function findStrikeRateOfEachBatsman(player, matchesArray, deliveriesArray) {
    const batsManWithBallRun = deliveriesArray
        .reduce((batsManWithBallRun, deliveryData) => {
            let batsman = deliveryData.batsman;
            let run = Number(deliveryData.batsman_runs);
            let id = deliveryData.match_id;
            let record = {};
            if (batsman == player) {
                if (id in batsManWithBallRun) {
                    record = batsManWithBallRun[id];
                    record.totalRun += run;
                    record.totalBalls += 1;
                    batsManWithBallRun[id] = record;
                }
                else {
                    record["totalRun"] = run;
                    record["totalBalls"] = 1;
                    batsManWithBallRun[id] = record;
                }
            }
            return batsManWithBallRun;
        }, {});
    //console.log(batsManWithBallRun);
    const batsmanPerSeason = matchesArray
        .reduce((batsmanPerSeason, element) => {
            let matchId = element.id;
            if (matchId in batsManWithBallRun) {
                let run = batsManWithBallRun[matchId].totalRun;
                let balls = batsManWithBallRun[matchId].totalBalls;
                let season = element.season;
                let record = {};
                if (season in batsmanPerSeason) {
                    record = batsmanPerSeason[season];
                    record.totalRun += run;
                    record.totalBalls += balls;
                    batsmanPerSeason[season] = record;

                }
                else {
                    record["totalRun"] = run;
                    record["totalBalls"] = balls;
                    batsmanPerSeason[season] = record;
                }
            }
            return batsmanPerSeason;
        }, {});
    //console.log(Object.entries(batsmanPerSeason));

    const strikeRatePerYear = Object.entries(batsmanPerSeason)
        .reduce((strikeRatePerYear, data) => {
            let run = data[1].totalRun;
            let balls = data[1].totalBalls;
            let strikeRate = ((run / balls) * 100).toFixed(2);
            strikeRatePerYear[data[0]] = strikeRate;
            //console.log(strikeRate);
            return strikeRatePerYear;
        }, {});
    // console.log(strikeRatePerYear);
    return strikeRatePerYear;
}

const findStrikeRateOfAllBatsman = (req, res, next) => {
    csvtojson().fromFile(deliveries)
        .then((deliveriesArray) => {
            csvtojson().fromFile(matches)
                .then((matchesArray) => {
                    const batsManWithBallRun = deliveriesArray
                        .reduce((batsManWithBallRun, deliveryData) => {
                            let batsman = deliveryData.batsman;
                            if (batsman in batsManWithBallRun) {
                            } else {
                                batsManWithBallRun[batsman] = findStrikeRateOfEachBatsman(batsman, matchesArray, deliveriesArray);
                            }
                            return batsManWithBallRun;
                        }, {});
                    res.status(302).json(batsManWithBallRun).end;
                })
                .catch((error) => {
                    console.error('Error occurred:', error);
                    next({
                        message: 'Internal Error Fetching Data',
                        status: 500
                    });
                });
        })
        .catch((error) => {
            console.error('Error occurred:', error);
            next({
                message: 'Internal Error Fetching Data',
                status: 500
            });
        });
}

module.exports = findStrikeRateOfAllBatsman;