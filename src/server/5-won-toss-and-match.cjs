const path = require('path');
const csvtojson = require('csvtojson');

const matches = path.join(__dirname, '../data/matches.csv');

const findTossAndMatchWinner = (req, res,next) =>{
    csvtojson().fromFile(matches)
        .then((matchesArray) => {
            const winnerData = matchesArray
                .reduce((wonMatchesAndToss, matchData) => {
                    let tossWinner = matchData.toss_winner;
                    let winner = matchData.winner;
                    if (tossWinner === winner) {
                        if (winner in wonMatchesAndToss) {
                            wonMatchesAndToss[winner]++;
                        }
                        else {
                            wonMatchesAndToss[winner] = 1;
                        }
                    }
                    return wonMatchesAndToss;
                }, {});
                res.status(302).json(winnerData).end;
        })
        .catch((error) => {
            console.error(error);
            next({
                message: 'Internal Error Fetching Data',
                status: 500
            });
        });
}

module.exports = findTossAndMatchWinner;