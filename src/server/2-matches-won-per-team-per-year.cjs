const path = require('path');
const csvtojson = require('csvtojson');

const matches = path.join(__dirname, '../data/matches.csv');

const matchesWonPerTeamPerYear = (req, res,next) => {
    csvtojson().fromFile(matches)
        .then((matchesArray) => {
            const matchesWonPerTeamPerYearAns = matchesArray
                .reduce((matchesWonPerTeamPerYear, matchData) => {
                    const winner = matchData.winner;
                    const season = matchData.season;
                    let newWinner = {};
                    if (season in matchesWonPerTeamPerYear) {
                        newWinner = matchesWonPerTeamPerYear[season]
                        if (winner in newWinner) {
                            newWinner[winner]++;
                        }
                        else {
                            newWinner[winner] = 1;
                        }
                    }
                    else {
                        newWinner[winner] = 1;
                        matchesWonPerTeamPerYear[season] = newWinner;
                    }
                    return matchesWonPerTeamPerYear;
                }, {});
            res.status(302).json(matchesWonPerTeamPerYearAns).end;
        })
        .catch((error) => {
            console.error('Error occurred:', error);
            next({
                message: 'Internal Error Fetching Data',
                status: 500
            });
        });
};

module.exports = matchesWonPerTeamPerYear;