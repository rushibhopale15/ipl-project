const path = require('path');
const fs = require('fs');
const csvtojson = require('csvtojson');

const matches = path.join(__dirname, '../data/matches.csv');
const deliveries = path.join(__dirname, '../data/deliveries.csv');

const jsonFilePath = path.join(__dirname, './../public/output/8-highest-dismissed-by-another.json');

const findHighestDismissalRecord = (req, res, next) => {
    csvtojson().fromFile(deliveries)
        .then((deliveriesArray) => {

            let dismisalInformation = deliveriesArray
                .reduce((dismisalInformation, delivaryData) => {
                    let dismisssedType = delivaryData.dismissal_kind;
                    if (dismisssedType !== '' && dismisssedType !== 'run out' && dismisssedType !== 'hit wicket' && dismisssedType !== 'retired hurt') {
                        let bowler = delivaryData.bowler;
                        let batsman = delivaryData.batsman;
                        let batsmanInfo = {};
                        if (bowler in dismisalInformation) {
                            batsmanInfo = dismisalInformation[bowler];
                            if (batsman in batsmanInfo) {
                                batsmanInfo[batsman]++;
                            }
                            else {
                                batsmanInfo[batsman] = 1;
                                dismisalInformation[bowler] = batsmanInfo;
                            }
                        }
                        else {
                            batsmanInfo[batsman] = 1;
                            dismisalInformation[bowler] = batsmanInfo;
                        }
                    }
                    return dismisalInformation;
                }, {});
            //console.log(Object.entries(dismisalInformation));
            //console.log(bowlerAndDismisselBatsman);    

            let bowlerBatsmanArray = Object.entries(dismisalInformation);
            let bowlerWithDismissal = bowlerBatsmanArray.map((bowlerData) => {
                //console.log(Object.entries(bowlerData[1]));
                batsman = Object.entries(bowlerData[1]).sort((batsman1, batsman2) => {
                    return batsman2[1] - batsman1[1];
                });
                return [bowlerData[0], batsman[0]];
            }).sort((bowlerData1, bowlerData2) => {
                return bowlerData2[1][1] - bowlerData1[1][1];
            })
            let highestDismissalPlayer = bowlerWithDismissal[0];
            // console.log(highestDismissalPlayer);

            const dismisalRecord = {};
            dismisalRecord["bowler"] = highestDismissalPlayer[0];
            dismisalRecord["batsman"] = highestDismissalPlayer[1][0];
            dismisalRecord["dismissalTime"] = highestDismissalPlayer[1][1];
            console.log(dismisalRecord);

            res.status(302).json(dismisalRecord).end;
        })
        .catch((error) => {
            console.error('Error occurred:', error);
            next({
                message: 'Internal Error Fetching Data',
                status: 500
            });
        });
}

module.exports = findHighestDismissalRecord;