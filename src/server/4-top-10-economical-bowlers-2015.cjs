const express = require('express');
const router = express.Router();
const path = require('path');
const csvtojson = require('csvtojson');

const matches = path.join(__dirname, '../data/matches.csv');
const deliveries = path.join(__dirname, '../data/deliveries.csv');

const findTopTenEconomicalBowler = (req, res, next) => {
    csvtojson().fromFile(matches)
        .then((matchesArray) => {
            csvtojson().fromFile(deliveries)
                .then((deliveriesArray) => {
                    let dataIdBySeason = matchesArray.filter((matchData) => {
                        return matchData.season === '2015';
                    }).map((matchData) => {
                        return matchData.id;
                    });

                    let bowlerAndRecord = deliveriesArray.reduce((bowlerObject, deliveryData) => {
                        if (dataIdBySeason.includes(deliveryData.match_id)) {
                            let currRun = Number(deliveryData.total_runs);
                            let bowler = deliveryData.bowler;
                            let bowlerInfo = {};
                            if (bowler in bowlerObject) {
                                bowlerInfo = bowlerObject[bowler];
                                bowlerInfo.run += currRun;
                                bowlerInfo.ball += 1;
                            } else {
                                bowlerInfo.ball = Number(deliveryData.ball);
                                bowlerInfo.run = currRun;
                                bowlerObject[bowler] = bowlerInfo;
                            }
                        }
                        return bowlerObject;
                    }, {});

                    let bowlerWithEconomy = Object.entries(bowlerAndRecord).reduce((bowlerObject, [bowlerName, bowlerData]) => {
                        let run = bowlerData.run;
                        let overs = bowlerData.ball / 6;
                        let economy = run / overs;
                        bowlerObject.push([bowlerName, economy.toFixed(2)]);
                        return bowlerObject;
                    }, []);

                    bowlerWithEconomy.sort((bowler1, bowler2) => bowler1[1] - bowler2[1]);
                    let tenBowlersArray = bowlerWithEconomy.slice(0, 10);
                    const firstTenEconomicalBowler = Object.fromEntries(tenBowlersArray);
                    res.status(302).json(firstTenEconomicalBowler).end;
                })
                .catch((error) => {
                    console.error(error);
                    next({
                        message: 'Internal Error Fetching Data',
                        status: 500
                    });
                });
        })
        .catch((error) => {
            console.error(error);
            next({
                message: 'Internal Error Fetching Data',
                status: 500
            });
        });
}

module.exports = findTopTenEconomicalBowler;