const path = require('path');
const csvtojson = require('csvtojson');
const matches = path.join(__dirname, '../data/matches.csv');

findMatchesPerYear = ((req, res,next)=>{
    csvtojson().fromFile(matches)
        .then((matchesArray) => {
            const matchesPerYear = matchesArray
                .reduce((accumulator, element) => {
                    let season = element.season;
                    //console.log(season);
                    if (season in accumulator) {
                        accumulator[season] += 1;
                    }
                    else {
                        accumulator[season] = 1;
                    }
                    return accumulator;
                }, {});
            res.status(302).json(matchesPerYear).end();
        })
        .catch((error) => {
            console.error('Error occurred:', error);
            next({
                message : 'Internal Error Fetching Data',
                status : 500
            });
        });
});

module.exports = findMatchesPerYear;