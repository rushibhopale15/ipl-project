const path = require('path');
const csvtojson = require('csvtojson');

const matches = path.join(__dirname, '../data/matches.csv');

const findPlayerOfMatchEachSeason = (req, res, next) => {
    csvtojson().fromFile(matches)
        .then((matchesArray) => {
            const playerOfMatchSeason = matchesArray
                .reduce((playersPerSeason, matchData) => {
                    let season = matchData.season;
                    let playerOfMatch = matchData.player_of_match;

                    let players = {};
                    if (season in playersPerSeason) {
                        players = playersPerSeason[season];
                        if (playerOfMatch in players) {
                            players[playerOfMatch]++;
                        }
                        else {
                            players[playerOfMatch] = 1;
                        }
                    }
                    else {
                        players[playerOfMatch] = 1;
                    }
                    playersPerSeason[season] = players;
                    return playersPerSeason;
                }, {});
            //console.log(Object.entries(playerOfMatchSeason));

            let playerOfMatchEachSeason = Object.entries(playerOfMatchSeason)
                .reduce((playerOfMatchSeason, [season, playerData]) => {
                    // console.log(playerData);
                    let playerName = '';
                    let maxTime = 0;
                    //console.log(Object.entries(playerData));
                    let record = Object.entries(playerData)
                        .reduce((maxPoS, [name, record]) => {
                            if (record > maxTime) {
                                maxTime = record;
                                playerName = name;
                            }
                            maxPoS = [playerName, maxTime];
                            return maxPoS;
                        }, {});
                    playerOfMatchSeason[season] = Object.fromEntries([record]);
                    return playerOfMatchSeason;
                }, {});
            //console.log(playerOfMatchEachSeason);
            res.status(302).json(playerOfMatchEachSeason).end;
        })
        .catch((error) => {
            console.error(error);
            next({
                message: 'Internal Error Fetching Data',
                status: 500
            });
        });
}

module.exports = findPlayerOfMatchEachSeason;
