const path = require('path');
const csvtojson = require('csvtojson');

const deliveries = path.join(__dirname, '../data/deliveries.csv');

const findBestEconomySuperOver = (req, res, next) => {
    csvtojson().fromFile(deliveries)
        .then((deliveriesArray) => {
            const bowlerRecords = deliveriesArray.reduce((bowlerRecords, deliveryData) => {

                if (deliveryData.is_super_over == '1') {
                    let bowler = deliveryData.bowler;
                    let currRuns = Number(deliveryData.total_runs);
                    let bowlsAndRuns = {};
                    if (bowler in bowlerRecords) {
                        bowlsAndRuns = bowlerRecords[bowler];
                        bowlsAndRuns.runs += currRuns;
                        bowlsAndRuns.balls += 1;
                        bowlerRecords[bowler] = bowlsAndRuns;
                    }
                    else {
                        bowlsAndRuns["runs"] = currRuns;
                        bowlsAndRuns["balls"] = 1;
                        bowlerRecords[bowler] = bowlsAndRuns;
                    }
                }
                return bowlerRecords;
            }, {});
            //console.log(Object.entries(bowlerRecords));

            let bowlerWithEconomy = Object.entries(bowlerRecords).map((bowlerData) => {
                let run = bowlerData[1].runs;
                let overs = bowlerData[1].balls / 6;
                let economy = (run / overs).toFixed(2);
                return [bowlerData[0], economy];
            }).sort((bowler1, bowler2) => {
                return bowler1[1] - bowler2[1]
            });
            //console.log(bowlerWithEconomy);

            let bowlersArray = bowlerWithEconomy.slice(0, 1); //stores first 1 bowler and their economy
            const bestEconomicalBowler = Object.fromEntries(bowlersArray);
            //console.log(bestEconomicalBowler);

            res.status(302).json(bestEconomicalBowler).end;
        })
        .catch((error) => {
            console.error('Error occurred:', error);
            next({
                message: 'Internal Error Fetching Data',
                status: 500
            });
        });
}

module.exports = findBestEconomySuperOver;