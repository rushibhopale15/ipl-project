const path = require('path');
const csvtojson = require('csvtojson');

const matches = path.join(__dirname, '../data/matches.csv');
const deliveries = path.join(__dirname, '../data/deliveries.csv');

const dataByTeam =  (req, res,next) => {
    csvtojson().fromFile(matches)
        .then((matchesArray) => {
            let idBy2016 = matchesArray.filter((matchData) => {
                return matchData.season === '2016';
            }).map((matchData) => {
                return matchData.id;
            });
            csvtojson().fromFile(deliveries)
                .then((deliveriesArray) => {
                    let dataByTeam = deliveriesArray.reduce((extraRunByTeam, deliveryData) => {
                        let extraRuns = Number(deliveryData.extra_runs);
                        let team = deliveryData.bowling_team;
                        if (idBy2016.includes(deliveryData.match_id)) {
                            if (team in extraRunByTeam) {
                                extraRunByTeam[team] += extraRuns;
                            } else {
                                extraRunByTeam[team] = extraRuns;
                            }
                        }
                        return extraRunByTeam;
                    }, {});
                    res.status(302).json(dataByTeam).end;
                })
                .catch((error) => {
                    console.error('Error occurred:', error);
                    next({
                        message: 'Internal Error Fetching Data',
                        status: 500
                    });
                });
        })
        .catch((error) => {
            console.error('Error occurred:', error);
            next({
                message: 'Internal Error Fetching Data',
                status: 500
            });
        });
};

module.exports = dataByTeam;
