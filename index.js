const express = require('express');
const app = express();
const {port} = require('./config');
const path = require('path');
const fs = require('fs');
const reqestId = require('express-request-id');
app.use(reqestId());


let logFilePath = path.join(__dirname,'./requests.log');
function createLogsFile(request,response,next){
    let data = `Request Id : ${request.id}, Request Method : ${request.method}, Request URL : ${request.url} \n`;
    fs.appendFile(logFilePath, data,(error)=>{
        if(error){
            next({
                message : 'Error while fetching requests',
                status : 404
            });
            console.error(error);
        }
    });
    next();
}
app.use(createLogsFile);



const matchesPerYear = require('./src/server/1-matches-per-year.cjs');
app.get('/matches_per_year',matchesPerYear);

const matchesWonPerTeamYear = require('./src/server/2-matches-won-per-team-per-year.cjs');
app.get('/matches_won_per_team_year', matchesWonPerTeamYear);

const dataByTeam = require('./src/server/3-extra-runs-per-team-2016.cjs');
app.get('/data_by_team', dataByTeam);

const topTenBowler = require('./src/server/4-top-10-economical-bowlers-2015.cjs');
app.get('/top_ten_bowler', topTenBowler);

const tossAndMatchWinner = require('./src/server/5-won-toss-and-match.cjs');
app.get('/winnerData', tossAndMatchWinner);

const playerOfMatchEachSeason = require('./src/server/6-highest-player-of-match.cjs');
app.get('/players_of_season', playerOfMatchEachSeason);

const findStrikeRateOfAllBatsman = require('./src/server/7-strike-rate-batsman-each-season.cjs');
app.get('/strike_rate_of_all_batsman', findStrikeRateOfAllBatsman);

const findHighestDismissalRecord = require('./src/server/8-highest-dismissed-by-another.cjs');
app.get('/highest_dismissal', findHighestDismissalRecord);

const findBestEconomySuperOver = require('./src/server/9-best-economy-in-super-overs.cjs');
app.get('/best_super_over', findBestEconomySuperOver);

app.get('/', (request, response) => {
    response.send(`
    <!DOCTYPE html>
        <html>
          <head>
          </head>
          <body></body><h1>Welcome to Home Page</h1>
    <h3>try these valid EndPoint :-</h3>
    <h4>/matches_per_year</h4>
    <h4>/matches_won_per_team_year</h4>
    <h4>/data_by_team</h4>
    <h4>/top_ten_bowler</h4>
    <h4>/winnerData</h4>
    <h4>/players_of_season</h4>
    <h4>/strike_rate_of_all_batsman</h4>
    <h4>/highest_dismissal</h4>
    <h4>/best_super_over</h4>
    <h4>/logs</h4>
    </body>
      </html>`);
});

app.get('/logs', (request,response)=>{
  response.sendFile(logFilePath);
});

const invalidRoute = (req, res, next) => {
    res.status(404).send({
      message: 'Enter valid route'
    });
  };
app.use(invalidRoute);

const handleError = (error, req, res, next) => {
    console.error('Error:', error);
    res.status(error.status || 500).json({
      error: error.message || 'Internal Server Error'
    });
    next();
};
app.use(handleError);

app.listen(port, () => {
    console.log(`server run on ${port}`);
});
